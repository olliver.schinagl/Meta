# Meta
This repository is used to document and define coding practices such as coding styles, writing commit messages and documentation.

The code_conventions.md defines the general coding conventions, without going into language specifics. It is possible that certain languages have their own coding standards. If this is the case, an additional document is added, which provides addendums or changes to the general codestyle.
