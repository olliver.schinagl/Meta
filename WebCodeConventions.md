JS/TS Code Conventions
=======

Our coding conventions:
* Use semi-colons
* 4 spaces for tabs
* Strings are single quotes
* camelCase for identifier names
* Use JSDoc style comments
* Private functions should start with an underscore (_)
* TS types should start with a capital letter
* New line per variable declaration


HTML Code Conventions
=======

Our coding conventions:
* 4 spaces for tabs
* Double quots for HTML attributes


SCSS Code Conventions
=======

Our coding conventions:
* 4 spaces for tabs
* BEM naming convention
